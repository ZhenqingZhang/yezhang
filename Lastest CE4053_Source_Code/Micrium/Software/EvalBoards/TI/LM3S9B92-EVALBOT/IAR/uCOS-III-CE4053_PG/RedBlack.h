#include <stdlib.h>

#include <includes.h>
#include <stdio.h>
#include "driverlib/timer.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include <os.h>



typedef int key_t;
typedef enum color_t
{
    RED = 0,
    BLACK = 1
}color_t;

typedef struct rb_node_t
{
    struct rb_node_t *left, *right, *parent;
    key_t key;
    color_t color;
    OS_TCB*  tcb;
    int  startTime;
}rb_node_t;

extern OS_MEM RBTreeBuffer;
//extern rb_node_t  *rb_t;

/* forward declaration */

rb_node_t* rb_insert(key_t key, int startTime,  rb_node_t* root, OS_TCB* tb);

rb_node_t* rb_search(key_t key, rb_node_t* root);

rb_node_t* rb_erase(key_t key, rb_node_t* root);