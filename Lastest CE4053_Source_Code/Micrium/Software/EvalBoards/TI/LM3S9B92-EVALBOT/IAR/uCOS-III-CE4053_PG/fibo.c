#include <stdio.h>
#include <math.h>
#include "fibo.h"
#include <os.h>

        int nH = 0;

        node *H = NULL;
        
     // extern   OS_MEM  FiBoNodeBuffer;     

       //   OS_ERR m_err;
       
/*

 * Initialize Heap

 */

node* InitializeHeap()

{

    node* np;

    np = NULL;

    return np;

}

/*

 * Create Node

 */

/*node* Create_node(TASK_TCB *a)

{

    node* x ; //(node*)OSMemGet((OS_MEM *)&MyPartition,(OS_ERR *)&m_err);

    x->data = a->data;

    return x;

}*/

/*

 * Insert Node

 */

int errorFlagInsert=0;

node* Insert(node* H, OS_TCB* tb,int data)

{
    //node *x = (node*)OSMemGet((OS_MEM *)&MyPartition,(OS_ERR *)&m_err);
   // x = Create_node(a);
 node* x=(node*)OSMemGet((OS_MEM *)&FiBoNodeBuffer,(OS_ERR *)&err);
 
 if(err!=OS_ERR_NONE)
   errorFlagInsert++;

    x->tcb = tb;
    x->data=data;

   // x->data = (OS_TICK)data;

    x->degree = 0;

    x->parent = NULL;

    x->child = NULL;

    x->left = x;

    x->right = x;

    x->mark = 'F';

    x->C = 'N';

    if (H != NULL)

    {

        (H->left)->right = x;

        x->right = H;

        x->left = H->left;

        H->left = x;

       if (x->data < H->data)

            H = x;

    }

    else

    {

        H = x;

    }

    nH = nH + 1;

    return H;

}

/*

 * Link Nodes in Fibonnaci Heap

 */

int Fibonnaci_link(node* H1, node* y, node* z)

{

    (y->left)->right = y->right;

    (y->right)->left = y->left;

    if (z->right == z)

        H1 = z;

    y->left = y;

    y->right = y;

    y->parent = z;

    if (z->child == NULL)

        z->child = y;

    y->right = z->child;

    y->left = (z->child)->left;

    ((z->child)->left)->right = y;

    (z->child)->left = y;

    if (y->data < (z->child)->data)

        z->child = y;

    z->degree++;

}

/*

 * Union Nodes in Fibonnaci Heap

 */

node* Union(node* H1, node* H2)

{

    node* np;

    node* H = InitializeHeap();

    H = H1;

    (H->left)->right = H2;

    (H2->left)->right = H;

    np = H->left;

    H->left = H2->left;

    H2->left = np;

    return H;

}

/*

 * Display Fibonnaci Heap

 */

int Display(node* H)

{
    //printf("The value if H is %d \n",H->n);//<<endl;

    node* p = H;

    if (p == NULL)

    {

        //printf("The Heap is Empty\n");

        return 0;

    }

    //printf("The root nodes of Heap are:\n ");

    do

    {

        //printf("%d",p->n);
        //printf("Now right: %d\n",p->right->n);//<<endl;
//cout<<H->n;


        p = p->right;

        if (p != H)

        {

            //printf("-->");

        }

    }

    while (p != H && p->right != NULL);

    //printf("\n");

}

/*

 * Extract Min Node in Fibonnaci Heap

 */

node *Provide_Min(node *H1)
{
  
    
    
    
}


void* _Extract_Min(node* H1)
 {
     node *node;
 
     if (H1==NULL)
         return 0;
 
     node = Extract_Min(H);
     if (node!=NULL)
     OSMemPut((OS_MEM  *)&FiBoNodeBuffer,                              
             (void    *)H1,                                  
            (OS_ERR  *)&err);
         //free(node);
     
}


node* Extract_Min(node* H1)

{

    node* p;

    node* ptr;

    node* z = H1;

    p = z;

    ptr = z;

    if (z == NULL)

        return z;

    node* x;

    node* np;

    x = NULL;

    if (z->child != NULL)

        x = z->child;

    if (x != NULL)

    {

        ptr = x;

        do

        {

            np = x->right;

            (H1->left)->right = x;

            x->right = H1;

            x->left = H1->left;

            H1->left = x;

            if (x->data < H1->data)

                H1 = x;

            x->parent = NULL;

            x = np;

        }

        while (np != ptr && np!=NULL);

    }

    (z->left)->right = z->right;

    (z->right)->left = z->left;

    H1 = z->right;

    if (z == z->right && z->child == NULL)

        H1 = NULL;

    else

    {

        H1 = z->right;

        H1=Consolidate(H1);

    }

    nH = nH - 1;

  //  return p;
   // return H1;
    
    return z;

}

/*

 * Consolidate Node in Fibonnaci Heap

 */

node* Consolidate(node* H1)

{

    int i,d;

  //  float f = (log(nH)) / (log(2));

//    int const D = f;
    
     int const D = 1;

    node* A[50] ;//OSMemGet((OS_MEM *)&MyPartition,(OS_ERR *)&m_err);

    for (i = 0; i <= D; i++)

        A[i] = NULL;

    node* x = H1;

    node* y;

    node* np;

    node* pt = x;

    do

    {

        pt = pt->right;

        d = x->degree;

        while (A[d] != NULL)

        {

            y = A[d];

            if (x->data > y->data)

            {

                np = x;

                x = y;

                y = np;

            }

            if (y == H1)

                H1 = x;

            Fibonnaci_link(H1, y, x);

            if (x->right == x)

                H1 = x;

                A[d] = NULL;

            d = d + 1;

        }

       

        x = x->right;
        
         A[d] = x;

    }

    while (x != H1 && x!=NULL);

    H = NULL;

    for (int j = 0; j <= D; j++)

    {

        if (A[j] != NULL)

        {

            A[j]->left = A[j];

            A[j]->right =A[j];

            if (H != NULL)

            {

                (H->left)->right = A[j];

                A[j]->right = H;

                A[j]->left = H->left;

                H->left = A[j];

                if (A[j]->data < H->data)

                H = A[j];

            }

            else

            {

                H = A[j];

            }

            if(H == NULL)

                H = A[j];

            else if (A[j]->data < H->data)

                H = A[j];

        }

    }
    
    return H1;

}

 

/*

 * Decrease key of Nodes in Fibonnaci Heap

 */

int Decrease_key(node*H1, int x, int k)

{

    node* y;

    if (H1 == NULL)

    {

        //printf("The Heap is Empty\n");

        return 0;

    }

    node* ptr = Find(H1, x);

    if (ptr == NULL)

    {

        //printf("Node not found in the Heap\n");

        return 1;

    }

    if (ptr->data > k)

    {

        

        return 0;

    }

    ptr->data = k;

    y = ptr->parent;

    if (y != NULL && ptr->data < y->data)

    {

        Cut(H1, ptr, y);

        Cascase_cut(H1, y);

    }

    if (ptr->data < H->data)

        H = ptr;

    return 0;

}

/*

 * Cut Nodes in Fibonnaci Heap

 */

int Cut(node* H1, node* x, node* y)

{

    if (x == x->right)

        y->child = NULL;

    (x->left)->right = x->right;

    (x->right)->left = x->left;

    if (x == y->child)

        y->child = x->right;

    y->degree = y->degree - 1;

    x->right = x;

    x->left = x;

    (H1->left)->right = x;

    x->right = H1;

    x->left = H1->left;

    H1->left = x;

    x->parent = NULL;

    x->mark = 'F';

}

 

/*

 * Cascade Cutting in Fibonnaci Heap

 */

int Cascase_cut(node* H1, node* y)

{

    node* z = y->parent;

    if (z != NULL)

    {

        if (y->mark == 'F')

        {

            y->mark = 'T';

}

        else

        {

            Cut(H1, y, z);

            Cascase_cut(H1, z);

        }

    }

}

 

/*

 * Find Nodes in Fibonnaci Heap

 */

node* Find(node* H, int k)

{

    node* x = H;

    x->C = 'Y';

    node* p = NULL;

    if (x->data == k)

    {

        p = x;

        x->C = 'N';

        return p;

    }

    if (p == NULL)

    {

        if (x->child != NULL )

            p = Find(x->child, k);

        if ((x->right)->C != 'Y' )

            p = Find(x->right, k);

    }

    x->C = 'N';

    return p;

}

/*

 * Delete Nodes in Fibonnaci Heap

 */

int Delete_key(node* H1, int k)

{

    node* np = NULL;

    int t;

    t = Decrease_key(H1, k, 9999999);

    if (!t)

        np = Extract_Min(H);

    if (np != NULL)

       ; //printf("Key Deleted\n");

    else

       ; //printf("Key not Deleted\n");
    OSMemPut((OS_MEM  *)&FiBoNodeBuffer,                              
             (void    *)H1,                                  
            (OS_ERR  *)&err);
    return 0;
     
          
     
}