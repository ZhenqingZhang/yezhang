#include <stdio.h>
#include <math.h>
#include <os.h>

//#define g_I2C_BusPtr_Port0 (&(g_I2C_Bus[0]))

extern OS_MEM  FiBoNodeBuffer;      
extern OS_ERR  err;
#define MQTY 10


//typedef struct task_tcb{
 // int period;
  //int data;
 // OS_TCB *p_tcb;
//}TASK_TCB;


typedef struct node

{

    
    int degree;
    struct node* parent;

    struct node* child;

    struct node* left;

    struct node* right;

    char mark;
    int  data;
    char C;
    OS_TCB* tcb;

}node;

typedef struct node Tree ;
//extern Tree   MyPartitionStorage[100];

        extern int nH;

        extern node *H;


        void *_Extract_Min(node* H1);
        node* InitializeHeap();

        int Fibonnaci_link(node*, node*, node*);

     //   node *Create_node(TASK_TCB*);

        node *Insert(node *, OS_TCB* tb,int data);

        node *Union(node *, node *);

        node *Extract_Min(node *);

        node* Consolidate(node *);

        int Display(node *);

        node *Find(node *, int);

        int Decrease_key(node *, int, int);

        int Delete_key(node *,int);

        int Cut(node *, node *, node *);
        
        node *Provide_Min(node *);

        int Cascase_cut(node *, node *);